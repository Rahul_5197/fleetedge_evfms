package com.qa.testcases;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;
import com.qa.base.TestBase;
import com.qa.pages.LoginPage;
import com.qa.pages.TrackReportPage;
import com.qa.pages.ViewAllPage;
import com.qa.util.TestUtil;

public class ViewAllVhicleSearchTest extends TestBase{
	
	LoginPage loginPage;
	TrackReportPage trackReportPage;
	ViewAllPage viewAllPage;
	String sheetName = "VIN";

	public ViewAllVhicleSearchTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	@BeforeMethod
	public void setUp() throws IOException, InterruptedException
	{
		initialization();
		loginPage = new LoginPage();
		TrackReportPage trackReportPage = loginPage.login(prop.getProperty("Username"), prop.getProperty("Password"));
		Thread.sleep(15000);
		viewAllPage = trackReportPage.navigateToViewAll();
		Thread.sleep(15000);
	}
	@DataProvider 
	public Object[][] getData() throws EncryptedDocumentException, InvalidFormatException, IOException{
		Object data[][] =	TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(dataProvider = "getData")
	public void searchVehicleTes(String vinumber) throws InterruptedException {
		viewAllPage.searchVehicle(vinumber);
	}
	

}
