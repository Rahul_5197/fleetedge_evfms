package com.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pages.LoginPage;
import com.qa.pages.TrackReportPage;

public class LoginPageTest extends TestBase{
	
	LoginPage loginPage;
	TrackReportPage trackReportPage;
	public LoginPageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@BeforeMethod()
	public void setUp() throws IOException
	{
		initialization();
		loginPage = new LoginPage();
	}
	
	@Test(priority = 1)
	public void validateLoginpageTitleTest() throws InterruptedException
	{
	String title =	loginPage.gateLoginpageTitle();
	String expectedTitle = "TataMotors: Command and Control";
	Assert.assertEquals(title, expectedTitle);
	Thread.sleep(3000);
	}
	
	@Test(priority = 2, retryAnalyzer = com.qa.util.RetryAnalyzer.class)
	public void logoVisisbleTest() throws InterruptedException
	{
		boolean b = loginPage.checkLoginPageLogo();
		Assert.assertTrue(b);
		Thread.sleep(3000);
	}
	
	@Test(priority = 3)
	public void loingTextTest() throws InterruptedException
	{
		boolean b = loginPage.checkloginPageText();
		Assert.assertTrue(b);
		Thread.sleep(3000);
	}
	
	@Test(priority = 4)
	public void loginIntoPortal() throws InterruptedException, IOException
	{
		trackReportPage = loginPage.login(prop.getProperty("Username"), prop.getProperty("Password"));
		
		Thread.sleep(5000);
	}

	@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}
}
