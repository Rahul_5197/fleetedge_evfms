package com.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pages.LoginPage;
import com.qa.pages.TrackReportPage;
import com.qa.pages.ViewAllPage;

public class TrackReportPageTest extends TestBase {
	
	LoginPage loginPage;
	TrackReportPage trackReportPage;
	ViewAllPage viewAllPage;

	public TrackReportPageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	@BeforeMethod
	public void setUp() throws InterruptedException, IOException
	{
		initialization();
		loginPage = new LoginPage();
		trackReportPage = 	loginPage.login(prop.getProperty("Username"), prop.getProperty("Password"));
		Thread.sleep(18000);
	}
	
	@Test(priority = 1)
	public void rackPageTitleTest() throws InterruptedException
	{
		String title = trackReportPage.getTrackReportPageTitle();
		String ExpectedTitle = "TataMotors: Command and Control";
		Assert.assertEquals(title, ExpectedTitle);
		Thread.sleep(2000);
	}
	
	@Test(priority = 2)
	public void logoTest() throws InterruptedException
	{
		boolean b = trackReportPage.logoOfTrackpage();
		Assert.assertTrue(b);
		Thread.sleep(2000);
	}
	
	@Test(priority = 3)
	public void navigationTest() throws IOException, InterruptedException
	{
		viewAllPage = 	trackReportPage.navigateToViewAll();
		Thread.sleep(4000);
	}
	
	@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}
}
