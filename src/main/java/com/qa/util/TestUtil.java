package com.qa.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.qa.base.TestBase;

public class TestUtil extends TestBase{
	
	public static long PAGE_LOAD_TIMEOUT = 30;
	public static long IMPLICIT_WAIT_TIMEOUT = 20;

	public TestUtil() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static String TESTDTA_SHEET_PATH= "C:\\Eclipse-Workspace\\FleetedgeEVAssist\\src\\main\\java\\com\\qa\\testdata\\TestData.xlsx";
	
	static Workbook book;
	static Sheet sheet;
	
	public static Object[][] getTestData(String sheetName) throws EncryptedDocumentException, IOException, InvalidFormatException{
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDTA_SHEET_PATH);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			book = WorkbookFactory.create(file);
			
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		sheet = book.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for(int i =0; i< sheet.getLastRowNum(); i++)
		{
			for(int j =0; j< sheet.getRow(0).getLastCellNum(); j++)
			{
				data[i][j] = sheet.getRow(i+1).getCell(j).toString(); 
			}
		}
				
		return data;
		
	}
	
	public static void takeScreenshot() throws IOException
	{
		File  src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String Currentdir = System.getProperty("user.dir");
		FileUtils.copyFile(src, new File(Currentdir+ "/screenshot/" + System.currentTimeMillis()+".png"));
	}

}
