package com.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.TestBase;

public class LoginPage extends TestBase{
	
	
	@FindBy(xpath = "//img[@src='assets/images/tm-logo-blue.svg']")
	WebElement assitEVLogo;
	
	@FindBy(xpath = "//h2[text()='Login']")
	WebElement loginHeader;
	
	@FindBy(xpath = "//h4[text()='Enter details registered with TATA Motors']")
	WebElement enterDetailsHeader;
	
	@FindBy(xpath = "//span[@ng-reflect-ng-class='pi pi-chevron-down']")
	WebElement selectRoleDropdown;
	
	@FindBy(xpath = "//span[@class='ng-star-inserted' and text()='SCV EV Admin']")
	WebElement SCVEVAdmin;
	
	@FindBy(xpath = "//input[@formcontrolname='emailId']")
	WebElement emailID;
	
	@FindBy(xpath = "//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath = "//input[@type='checkbox']")
	WebElement checkBox;
	
	@FindBy(xpath = "//button[@id='submiLogi']")
	WebElement loginbtn;
	
	public LoginPage() throws IOException {
		super();
		PageFactory.initElements(driver, this);
	}

	public String gateLoginpageTitle()
	{
	String title = 	driver.getTitle();
	return title;
	}
	
	public boolean checkLoginPageLogo()
	{
		boolean b = assitEVLogo.isDisplayed();
		return b;
	}
	
	public boolean checkloginPageText() {
		boolean b = enterDetailsHeader.isDisplayed();
		return b;
	}
	
	public TrackReportPage login(String Username, String Password) throws InterruptedException, IOException
	{
		selectRoleDropdown.click();
		Thread.sleep(1000);
		SCVEVAdmin.click();
		Thread.sleep(1000);
		emailID.sendKeys(Username);
		password.sendKeys(Password);
		checkBox.click();
		loginbtn.click();
		return new TrackReportPage();
	}
}
