package com.qa.pages;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.base.TestBase;

public class TrackReportPage extends TestBase {
	
	public static WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
	@FindBy(xpath = "//img[@src='assets/images/tm-logo.svg']")
	WebElement logo;
	
	@FindBy(xpath = "//span[text()='Track Report']")
	WebElement trackReport;
	
	@FindBy(xpath = "//span[text()='User']")
	WebElement user;
	
	@FindBy(xpath = "//button[@id='view-all']")
	WebElement viewAll;

	public TrackReportPage() throws IOException {
		super();
		PageFactory.initElements(driver,this);
		
	}
	
	public String getTrackReportPageTitle()
	{
		String title = driver.getTitle();
		return title;
	}
	
	public boolean logoOfTrackpage()
	{
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@src='assets/images/tm-logo.svg']")));
		
		boolean b = logo.isDisplayed();
		return b;
	}
	
	public ViewAllPage navigateToViewAll() throws IOException
	{
		wait.until(ExpectedConditions.elementToBeClickable(viewAll)).click();
		return new ViewAllPage();
	}

}
