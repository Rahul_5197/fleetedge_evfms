package com.qa.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.TestBase;

public class ViewAllPage extends TestBase {

	@FindBy(xpath = "//button[@class='btn btn-icon btn-search ng-star-inserted']")
	WebElement searchBar;
	
	@FindBy(xpath = "//input[@placeholder='Search']")
	WebElement searchInput;
	
	@FindBy(xpath = "//button[@class='btn-search-button']")
	WebElement searchBtn;
	
	
	
	public ViewAllPage() throws IOException {
		super();
		PageFactory.initElements(driver,this);
	}
	
	
	public void searchVehicle(String VIN) throws InterruptedException
	{
		searchBar.click();
		Thread.sleep(2000);
		searchInput.sendKeys(VIN);
		Thread.sleep(1000);
		searchBtn.click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//span[contains(@class,'clickable ng-star-inserted') and text()=' "+VIN+" ']")).click();
	}
	

}
